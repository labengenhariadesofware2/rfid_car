import android
var initialLocation;
var siberia = new google.maps.LatLng(60, 105);
var newyork = new google.maps.LatLng(40.69847032728747, -73.9514422416687);
var browserSupportFlag =  new Boolean();

function initialize() {
var mapCanvas = document.getElementById('map_canvas');
var mapOptions = {
  center: new google.maps.LatLng(44.5403, -78.5463),
  zoom: 15,
  mapTypeId: google.maps.MapTypeId.ROADMAP
}

var map = new google.maps.Map(mapCanvas, mapOptions)

var marker = new google.maps.Marker({
  title: "Seu veiculo se encontra aqui"
});

if(!!navigator.geolocation) {
  browserSupportFlag = true;
  navigator.geolocation.getCurrentPosition(function(position) {
    initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
    map.setCenter(initialLocation);
    marker.setPosition(initialLocation);
    marker.setMap(map);
  }, function() {
      handleNoGeolocation(browserSupportFlag);
  });

} 
// Browser doesn't support Geolocation
else {
  browserSupportFlag = false;
  handleNoGeolocation(browserSupportFlag);
}

function handleNoGeolocation(errorFlag) {
  if (errorFlag == true) {
    alert("Geolocation service failed.");
    initialLocation = newyork;
  } 
  else {
    alert("Your browser doesn't support geolocation. We've placed you in Siberia.");
    initialLocation = siberia;
  }
  map.setCenter(initialLocation);
  marker.setPosition(initialLocation);
  marker.setMap(map);
}



}
google.maps.event.addDomListener(window, 'load', initialize);

