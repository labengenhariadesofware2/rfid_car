//
// RFID reader function testing
// based on http://arduino.cc/playground/Code/ID12
//

#include <SoftwareSerial.h>

SoftwareSerial rfidReaderSerial(2, 3); // RX, TX

int ledCheck = 13;

void info() {
  digitalWrite(ledCheck, HIGH);
  delay(30);
  digitalWrite(ledCheck, LOW);
  delay(30);
}

void setup() {
  rfidReaderSerial.begin(9600);
  Serial.begin(9600);
  pinMode(ledCheck, OUTPUT);
}

void loop () {
  byte i = 0;
  byte val = 0;
  byte code[6];
  byte checksum = 0;
  byte bytesread = 0;
  byte tempbyte = 0;

  if(rfidReaderSerial.available() > 0) {
    info();
    if((val = rfidReaderSerial.read()) == 2) {                  // checa o cabeçalho 
      info();
      bytesread = 0; 
      while (bytesread < 12) {                        // le os 10 digitos do codigo + 2 do checksum
        if( rfidReaderSerial.available() > 0) { 
          val = rfidReaderSerial.read();
          info();
          if((val == 0x0D)||(val == 0x0A)||(val == 0x03)||(val == 0x02)) { // se cabeçalho ou stop bytes antes da leitura dos 10 digitos 
            break;                                    // pare de ler
          }

          // Conversao Ascii/Hex
          if ((val >= '0') && (val <= '9')) {
            val = val - '0';
          } 
          else if ((val >= 'A') && (val <= 'F')) {
            val = 10 + val - 'A';
          }

          // A cada dois digitos em hex, adiciona um byte ao codigo:
          if (bytesread & 1 == 1) {
            // cria espaco para o digito em hex
            // shiftando o digito hex anterior 4 bits para a esquerda
            code[bytesread >> 1] = (val | (tempbyte << 4));

            if (bytesread >> 1 != 5) {                // Se chegamos ao byte de checksum,
              checksum ^= code[bytesread >> 1];       // Calcula o checksum (XOR)
            };
          } 
          else {
            tempbyte = val;                           // Armazena o primeiro digito em hex
          };

          bytesread++;                                // se prepara para o proximo digito
        } 
      } 

      // Output to Serial:

      if (bytesread == 12) {                          // se a leitura dos 12 digitos esta completa
        Serial.print("5-byte code: ");
        for (i=0; i<5; i++) {
          if (code[i] < 16) Serial.print("0");
          Serial.print(code[i], HEX);
          Serial.print(" ");
        }
        Serial.println();

        Serial.print("Checksum: ");
        Serial.print(code[5], HEX);
        Serial.println(code[5] == checksum ? " -- passed." : " -- error.");
        Serial.println();
      }

      bytesread = 0;

    }

  }
  
}

